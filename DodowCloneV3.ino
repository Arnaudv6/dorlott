/* Dodow Arduino Clone (on my teensy 3.0) by arnaudv6.
https://github.com/arnaudv6/Dodow-Clone
Most credit goes to Gary909 for his work, on which this is shamelessly based:
https://github.com/gary909/Dodow-Clone

Rough assumptions: falling to sleep is going from 11 bpm (breaths per minute)
down to 6 bpm, linearly in 6 minutes, and maintaining the rate a little more.

This is rough estimation though, as those rates vastly depend on age, mainly.
See: https://en.wikipedia.org/wiki/Respiratory_rate
*/


#include <avr/sleep.h>

int pinNumber = 33;
int ledPins[] = {3, 6, 9};       // LEDs welded on those pins
int ledCount = sizeof(ledPins) / sizeof(int);

// adjust those at your convenience, see rationale in header
// I did not implement any preprocessor assertions.
// setting a phase above 254 minutes might cause unexpected behavior
float startBpm = 11;
float lowBpm = 6;
float minutesToLowBpm = 6;
float minutesAtLowBpm = 10;
float minutesDimmingLight = 3;
float minLightIntensity = 0.1;

float pause = 970;               // ms, inbetween breaths
float ratioInEx = 1.23;          // ratio inhaleTime/exhaleTime

// easy deductions:
int breathsAtLowBPM = int(minutesAtLowBpm * lowBpm);
int breathsLoweringLight = int(minutesDimmingLight * lowBpm);
float lightIntensityStepDownPerBreath = (1 - minLightIntensity) / breathsLoweringLight;

// decreasing BPM linearly (good enougth):
float bpmGap = startBpm - lowBpm;
float averageBpm = (startBpm + lowBpm) / 2;
float bpmStepDownPerBreath = (bpmGap / minutesToLowBpm) / averageBpm; // positive nbr

// same info, two types.
int brightness;
float brightnessFloat;
float exhaling;
float inhaling;

void setup() {                   // setup: runs on power-on (or reset button pushed)
    // setting pins in <output> mode to save power.
    // https://www.pjrc.com/teensy/low_power.html
    for (int pin = 0; pin <= pinNumber; pin++){
        pinMode(pin, OUTPUT);
    }

    // let's breathe slower and slower. reasonably.
    for (float bpm = startBpm; bpm > lowBpm; bpm-=bpmStepDownPerBreath){
        float period = (60000 / bpm) - pause;
        exhaling = period / (1 + ratioInEx);
        inhaling = period - exhaling;

        inhale(exhaling, 1.0);
        exhale(inhaling, 1.0);
        delay(pause);
    }

    // let's breathe a while at slow rate.
    for (int x = 1; x < breathsAtLowBPM; x++){
        inhale(exhaling, 1.0);
        exhale(inhaling, 1.0);
        delay(pause);
    }

    // let's switch the lights off nicely.
    for (float x = 1; x >= minLightIntensity; x -= lightIntensityStepDownPerBreath){
        inhale(exhaling, x);
        exhale(inhaling, x);
        delay(pause);
    }

    // then hibernate the device to save power.
    // Serial.end();             // shut off USB
    // ADCSRA = 0;               // shut off ADC
    set_sleep_mode (SLEEP_MODE_PWR_DOWN);
    sleep_enable();
    sleep_cpu ();
}

void inhale(float inhalingTime, float lightPowerCoeff) {
    // magic numbers, found empirically, see *.ods in program's repository.
    float coeffIn = 1 + ( 19.5 / inhalingTime );
    float combiIn = 800 / inhalingTime;

    brightnessFloat = 1.0;       // starting from 1 and multiplying
    while (brightnessFloat <= 255){
        if (brightnessFloat > 254){
            brightness = int(254 * lightPowerCoeff);  // ceilling it all
        }
        else {
            brightness = int(brightnessFloat * lightPowerCoeff);
        }
        for(int index = 0; index < ledCount; index++)
        {
            analogWrite(ledPins[index], brightness);
        }
        delay(10);
        brightnessFloat = brightnessFloat * coeffIn + combiIn;
    }
}

void exhale(float exhalingTime, float lightPowerCoeff) {
    // magic numbers, found empirically, see *.ods in program's repository.
    float coeffEx = 1 + ( 31 / exhalingTime );
    float combiEx = - 340 / exhalingTime;
    // combiEx is negative

    brightnessFloat = 254.0;     // starting from 254 and multiplying
    while (brightnessFloat >= 0){
        if (brightnessFloat < 0){
            brightness = 0;      // flooring the int var, here.
        }
        else {
            brightness = int(brightnessFloat * lightPowerCoeff);
        }
        for(int index = 0; index < ledCount; index++)
        {
            analogWrite(ledPins[index], brightness);
        }
        delay(10);
        brightnessFloat = brightnessFloat / coeffEx + combiEx;
    }
}

void loop() {
    // put your main code here, to run repeatedly
}
