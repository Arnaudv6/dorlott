# Dorlott
A [Dodow](https://www.mydodow.com/) DIY Clone for Arduino  
heavilly based on https://github.com/gary909/Dodow-Clone  
I only made the soft a lot more flexible.

<img src="shot.gif">

# Hardware used
 * An arduino board (I used a Teensy 3.0 I had lying around)
 * LEDs:  
    Cree® 5-mm Amber Round LED C503B-AAS/AAN-015  
    because of detailed specs available, and the optical quality, color and all...  
	2.6Vmax 0.2A. So Teensy's voltage (3.3V) is about 0.7V over.  
	Given its 0.01A, we need 70ohm resistors  
	https://www.conrad.com/ce/ProductDetail.html?productcode=1125332  
    sorry about the dead link
 * Resistors  
	https://www.conrad.com/ce/en/product/1417678/
 * Reflector (I ended up not using one)  
	https://www.conrad.fr/ce/fr/product/185296/
 * Power resistor  
    1v @ 0.3A : 3.33ohm  
	I went for a 0.25A, 4 ohm  
	https://www.conrad.com/ce/en/product/1473110

# Tutorial
Please refer to Gary's [illustrated guide](http://www.instructables.com/id/DIY-Dodow-Clone-Arduino-Sleep-Meditation-Machine/) on instructables.com.
